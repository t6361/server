fastapi==0.70.1
uvicorn==0.16.0
starlette==0.16.0
aiofiles==0.8.0
pydantic==1.8.2