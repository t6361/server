from fastapi import HTTPException

__all__ = ["BaseApiException"]


class BaseApiException(HTTPException):
    message: str = ...
    status_code: int = ...

    def __init__(self):
        super().__init__(status_code=self.status_code, detail=self.message)

    def build_docs(self):
        return {
            str(self.status_code): {
                "content": {"application/json": {"example": {"message": self.message}}},
            },
        }
