from typing import TypeVar

from pydantic import BaseModel, SecretStr

__all__ = ["BaseApiModel", "Model"]

Model = TypeVar("Model", bound="BaseApiModel")


class BaseApiModel(BaseModel):
    """Base model for api."""

    class Config:
        use_enum_values = True
        json_encoders = {
            SecretStr: lambda v: v.get_secret_value() if v else None,
        }
