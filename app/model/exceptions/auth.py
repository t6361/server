from app.model.base import BaseApiException
from fastapi import status

__all__ = ["IncorrectTokenLength", "UserNameTooShort"]


class IncorrectTokenLength(BaseApiException):
    message = "Не правильная длина токена"
    status_code = status.HTTP_400_BAD_REQUEST


class UserNameTooShort(BaseApiException):
    message = "Имя пользователя слишком короткое."
    status_code = status.HTTP_400_BAD_REQUEST
