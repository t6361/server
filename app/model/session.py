import uuid

from pydantic import Field, SecretStr

from app.model.base import BaseApiModel

__all__ = ["Session", "CreateSessionCommand"]


class BaseSession(BaseApiModel):
    """Base session model"""


class Session(BaseSession):
    id: str = Field(default_factory=lambda _: uuid.uuid4().__str__())
    password: SecretStr


class CreateSessionCommand(BaseApiModel):
    token: SecretStr
    password: SecretStr
