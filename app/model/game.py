from pydantic import PositiveInt, SecretStr

from .base import BaseApiEnum, BaseApiModel

__all__ = ["GetGameFieldCommand", "MakeMoveCommand"]


class BaseGame(BaseApiModel):
    """Base chess model."""


class GetGameFieldCommand(BaseGame):
    token: SecretStr
    password: SecretStr


class MoveType(BaseApiEnum):
    MOVE_X = "x"
    MOVE_O = "o"


class MakeMoveCommand(BaseGame):
    token: SecretStr
    password: SecretStr
    index: int
    move: MoveType
