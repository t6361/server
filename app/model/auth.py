from typing import Type

from pydantic import SecretStr, validator

from .base import BaseApiModel, Model
from .exceptions import IncorrectTokenLength, UserNameTooShort


__all__ = [
    "AuthLoginResponse",
    "AuthRegistrationResponse",
    "AuthLoginCommand",
    "AuthRegistrationCommand",
]


class BaseAuth(BaseApiModel):
    """Base auth model"""


class AuthLoginResponse(BaseAuth):
    username: str


class AuthRegistrationResponse(BaseAuth):
    token: SecretStr


class AuthLoginCommand(BaseAuth):
    token: SecretStr

    @validator("token")
    def check_token_length(cls: Type["Model"], value: SecretStr):
        if value.get_secret_value().__len__() < 16:
            raise IncorrectTokenLength
        return value


class AuthRegistrationCommand(BaseAuth):
    username: str

    @validator("username")
    def check_token_length(cls: Type["Model"], value: str):
        if value.__len__() < 2:
            raise UserNameTooShort
        return value
