from fastapi import FastAPI
from .configuration import FastApiServer, settings


def create_app():
    app = FastAPI()
    return FastApiServer(app=app).app
