import random
from typing import List

from fastapi import APIRouter, status

from .exceptions import IncorrectSessionId, UnknownUser
from ..model.auth import AuthLoginCommand
from ..model.session import CreateSessionCommand, Session
from ..file_storage import fs

router = APIRouter(
    prefix="/api/session", tags=["Session"], responses={404: {"description": "Not found"}}
)


@router.post(
    "/",
    status_code=status.HTTP_200_OK,
    response_model=Session,
    response_model_exclude={"password"},
)
async def create_session(cmd: CreateSessionCommand):
    db_read = await fs.read()
    if cmd.token.get_secret_value() not in db_read:
        raise UnknownUser
    user_data = db_read[cmd.token.get_secret_value()]
    if not user_data.get("sessions", None):
        max_session_number = 1
        user_data["sessions"] = {
            max_session_number: {
                "id": int("".join(str(random.randint(1, 9)) for _ in range(10))),
                "password": cmd.password.get_secret_value(),
            },
        }
    else:
        max_session_number = int(max(user_data["sessions"], key=int)) + 1
        user_data["sessions"] = {
            **user_data["sessions"],
            max_session_number: {
                "id": int("".join(str(random.randint(1, 9)) for _ in range(10))),
                "password": cmd.password.get_secret_value(),
            },
        }
    await fs.update(for_update={cmd.token.get_secret_value(): user_data})
    return Session(id=max_session_number, password=cmd.password)


@router.post(
    "/{session_id:int}",
    responses={**IncorrectSessionId().build_docs()},
    response_model_exclude={"id"},
)
async def get_session(session_id: int, at: AuthLoginCommand):
    for token, user_data in (await fs.read()).items():
        if at.token.get_secret_value() != token:
            continue
        for item in user_data["sessions"].values():
            if session_id != item["id"]:
                continue
            return Session(id=session_id, password=item["password"])
        raise IncorrectSessionId
    raise UnknownUser


@router.post(
    "/all",
    status_code=status.HTTP_200_OK,
    response_model=List[Session],
    response_model_exclude={"password"},
)
async def get_all_sessions(at: AuthLoginCommand):
    r = await fs.read()
    if not (r := r.get(at.token.get_secret_value(), None)):
        raise UnknownUser
    return [Session(**i) for i in list(r["sessions"].values())]
