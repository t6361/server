from . import session, auth, game


__routes__ = [game.router, auth.router, session.router]