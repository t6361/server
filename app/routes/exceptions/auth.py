from fastapi import status
from ...model.base import BaseApiException

__all__ = ["UserAlreadyExist", "UnknownUser"]


class UserAlreadyExist(BaseApiException):
    message = "Пользователь с таким именем уже существует"
    status_code = status.HTTP_409_CONFLICT


class UnknownUser(BaseApiException):
    message = "Неизвестный токен"
    status_code = status.HTTP_400_BAD_REQUEST
