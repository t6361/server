from app.model.base import BaseApiException
from fastapi import status

__all__ = ["IncorrectSessionId", "IncorrectPassword", "IncorrectSessionIdOrPassword"]


class IncorrectSessionId(BaseApiException):
    message = "Неправильный идентификатор сессии."
    status_code = status.HTTP_400_BAD_REQUEST


class IncorrectPassword(BaseApiException):
    message = "Неправильный пароль."
    status_code = status.HTTP_401_UNAUTHORIZED


class IncorrectSessionIdOrPassword(BaseApiException):
    message = "Неправильный идентификатор сессии или пароль."
    status_code = status.HTTP_400_BAD_REQUEST
