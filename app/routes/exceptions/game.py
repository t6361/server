from app.model.base import BaseApiException
from fastapi import status

__all__ = ["GameWasNotStarted"]


class GameWasNotStarted(BaseApiException):
    status_code = status.HTTP_400_BAD_REQUEST
    message = "Игра еще не началась."
