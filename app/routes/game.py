from itertools import product
from typing import Any, Dict, List

from fastapi import APIRouter, status
from starlette.websockets import WebSocket, WebSocketDisconnect

from .exceptions import (
    IncorrectSessionIdOrPassword,
)
from .exceptions.game import GameWasNotStarted
from ..file_storage import fs
from ..model.game import MakeMoveCommand, GetGameFieldCommand, MoveType

router = APIRouter(
    prefix="/api/game", tags=["Game"], responses={404: {"description": "Not found"}}
)


class ConnectionManager:
    def __init__(self):
        self.active_connections: Dict[int, List[WebSocket]] = {}

    async def connect(self, websocket: WebSocket, session_id: int):
        await websocket.accept()
        self.active_connections[session_id].append(websocket)

    def disconnect(self, websocket: WebSocket, session_id: int):
        self.active_connections[session_id].remove(websocket)

    async def broadcast(self, message: Dict[str, Any], session_id: int):
        for connection in self.active_connections.get(session_id, []):
            await connection.send_json(message)


manager = ConnectionManager()


@router.post(
    "/{session_id:int}", status_code=status.HTTP_200_OK, response_model=List[str]
)
async def get_game_field(session_id: int, cmd: GetGameFieldCommand):
    r = await fs.read()
    if cmd.token.get_secret_value() not in r:
        raise IncorrectSessionIdOrPassword
    session = r[cmd.token.get_secret_value()].pop("sessions")
    for index, __session in session.items():
        if not (
            __session["id"] == session_id
            and __session["password"] == cmd.password.get_secret_value()
        ):
            continue
        __session["game_field"] = __session.get("game_field", [""] * 9)
        await fs.update(
            for_update={
                cmd.token.get_secret_value(): {
                    **r[cmd.token.get_secret_value()],
                    **{"sessions": {**session, index: __session}},
                }
            }
        )
        return __session["game_field"]
    raise IncorrectSessionIdOrPassword


@router.post(
    "/{session_id:int}/move", status_code=status.HTTP_200_OK, response_model=List[str]
)
async def make_move(session_id: int, cmd: MakeMoveCommand):
    r = await fs.read()
    if cmd.token.get_secret_value() not in r:
        raise IncorrectSessionIdOrPassword
    session = r[cmd.token.get_secret_value()].pop("sessions")
    for index, __session in session.items():
        if not (
            __session["id"] == session_id
            and __session["password"] == cmd.password.get_secret_value()
        ):
            continue
        game_field = __session["game_field"]
        if not game_field:
            raise GameWasNotStarted
        for i, v in product(range(3), [[MoveType.MOVE_X] * 3, [MoveType.MOVE_O] * 3]):
            if (
                game_field[i::3] == v
                or game_field[i * 3 : i * 3 + 3] == v
                or (i and (game_field[::4] == v or game_field[2:7:2] == v))
            ):
                await manager.broadcast(
                    message={r[cmd.token.get_secret_value()]["username"]: game_field},
                    session_id=session_id,
                )
                return [MoveType.MOVE_X] * 9 if (v[0] == "x") else [MoveType.MOVE_O] * 9
        game_field[cmd.index] = cmd.move
        await manager.broadcast(
            session_id=session_id,
            message={r[cmd.token.get_secret_value()]["username"]: game_field},
        )
        await fs.update(
            for_update={
                cmd.token.get_secret_value(): {
                    **r[cmd.token.get_secret_value()],
                    **{"sessions": {**session, index: __session}},
                }
            }
        )
        return game_field
    raise IncorrectSessionIdOrPassword


@router.websocket("/ws/{session_id:int}")
async def websocket_endpoint(websocket: WebSocket, session_id: int):
    await manager.connect(websocket, session_id=session_id)
    try:
        while True:
            await websocket.receive()
    except WebSocketDisconnect:
        manager.disconnect(session_id=session_id, websocket=websocket)
