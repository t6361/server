import uuid

from fastapi import APIRouter, status

from .exceptions import UnknownUser, UserAlreadyExist
from ..model.auth import (
    AuthLoginResponse,
    AuthRegistrationResponse,
    AuthLoginCommand,
    AuthRegistrationCommand,
)
from ..model.exceptions import IncorrectTokenLength, UserNameTooShort
from ..file_storage import fs

router = APIRouter(
    prefix="/api/auth", tags=["Auth"], responses={404: {"description": "Not found"}}
)


@router.post(
    "/login",
    status_code=status.HTTP_200_OK,
    response_model=AuthLoginResponse,
    responses={**IncorrectTokenLength().build_docs(), **UnknownUser().build_docs()},
)
async def login(cmd: AuthLoginCommand):
    r = await fs.read()
    if cmd.token.get_secret_value() not in r:
        raise UnknownUser
    return AuthLoginResponse(username=r[cmd.token.get_secret_value()]["username"])


@router.post(
    "/registration",
    status_code=status.HTTP_200_OK,
    responses={**UserNameTooShort().build_docs(), **UserAlreadyExist().build_docs()},
)
async def registration(cmd: AuthRegistrationCommand):
    for usernames in (await fs.read()).values():
        if usernames["username"] == cmd.username:
            raise UserAlreadyExist
    await fs.update(
        for_update={(u := uuid.uuid4().__str__()): {"username": cmd.username}}
    )
    return AuthRegistrationResponse(token=u)
