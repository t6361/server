from starlette.requests import Request
from starlette.responses import JSONResponse
from ..model.base import BaseApiException

__all__ = ["handle_exceptions"]


def handle_exceptions(request: Request, exc: BaseApiException):
    _ = request

    return JSONResponse(status_code=exc.status_code, content={"message": exc.message})
