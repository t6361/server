import json
from pathlib import Path
from typing import Any, Dict

from ..configuration import get_settings
import aiofiles


class FileStorage:
    file_path: Path

    def __init__(self):
        settings = get_settings()
        Path(settings.FILE_STORAGE_PATH).mkdir(parents=True, exist_ok=True)
        self.file_path = Path(
            settings.FILE_STORAGE_PATH, settings.FILE_STORAGE_FILE_NAME
        )

    async def read(self) -> Dict[str, Any]:
        async with aiofiles.open(self.file_path, mode="r") as f:
            contents = await f.read()
            try:
                return json.loads(contents)
            except json.JSONDecodeError:
                return {}

    async def update(self, for_update: Dict[str, Any]):
        async with aiofiles.open(self.file_path, mode="r") as f:
            try:
                contents = await f.read()
                data = json.loads(contents)
            except json.JSONDecodeError:
                data = {}
            data.update(for_update)
            json_content = json.dumps(data)

        async with aiofiles.open(self.file_path, mode="w") as f:
            await f.write(json_content)
