from typing import TypeVar

from ..middlewares import handle_exceptions
from ..model.base import BaseApiException
from ..routes import __routes__

FastAPIInstance = TypeVar("FastAPIInstance", bound="FastAPI")

__all__ = ["FastApiServer"]


class FastApiServer:
    def __init__(self, app: FastAPIInstance):
        self.__app = app
        self.__register_routes(app)

    @property
    def app(self):
        return self.__app

    @staticmethod
    def __register_routes(app: FastAPIInstance):
        for route in __routes__:
            app.include_router(router=route)

    @staticmethod
    def __register_http_exception_handler(app: FastAPIInstance):
        app.add_exception_handler(BaseApiException, handle_exceptions)

