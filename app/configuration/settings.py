from functools import lru_cache
from pathlib import Path
from typing import Any, Dict, Type

from pydantic import BaseSettings, root_validator, validator

__all__ = ["get_settings"]

from app.model.base import Model


class Settings(BaseSettings):
    FILE_STORAGE_PATH: Path = Path(__name__).absolute().parent.joinpath("storage")
    FILE_STORAGE_FILE_NAME: str = "db.json"

    @root_validator
    def check_file_exist(cls: Type["Model"], values: Dict[str, Any]):
        values["FILE_STORAGE_PATH"].mkdir(exist_ok=True, parents=True)
        values["FILE_STORAGE_PATH"].joinpath(values["FILE_STORAGE_FILE_NAME"]).touch(
            exist_ok=True
        )
        return values


@lru_cache()
def get_settings() -> Settings:
    return Settings()
