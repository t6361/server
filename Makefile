## Default target
.DEFAULT_GOAL := run

run:
	uvicorn app:create_app --host localhost --reload --port 5000
